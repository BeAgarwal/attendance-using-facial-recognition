import mysql.connector as mysql
from datetime import datetime
from datetime import date
import sys
import MysqlConnection

def markAttendance(name, designation):
    conn = MysqlConnection.connectwithMysql()
    date_object = date.today()
    now = datetime.now()
    time_object = now.strftime("%H:%M:%S")
    cur = conn.cursor()
    sql = "DELETE FROM attendance WHERE NAME = (%s) AND DESIGNATION = (%s)"
    cur.execute(sql, (name, designation))
    sql = "INSERT INTO attendance(name, date, time, designation) VALUES(%s, %s, %s, %s)"
    cur.execute(sql, (name, date_object, time_object, designation))
    conn.commit()

    conn.close()
    return True