# Attendance using Facial Recognition
#### Feel free to fork, download, or pull. If you have any problem, contact me. 😀
## Developer(s): 
 - SHUBHAM AGARWAL [LinkedIn](https://linkedin.com/in/beagarwal) 

## Introduction :
This project intends to serve as an efficient substitute
for traditional manual attendance systems. It can be used in corporate colleges, schools, and
organizations where security is essential.This project serves as a foundation for future projects based on facial detection and recognition.
This project also covers web development and database management for a user- friendly UI.

## Project Structure:



   *  We followed MTV architecture for the whole project.
   *  The views, models, and templates are thus separated, allowing us to harness the power of
MTV architecture. 
   * Our database models are defined inside the python API which inturn stores the data using the MySQL. 
   * The templates contain HTML files which create the UI layer.The HTML contents along with linked CSS, Bootstrap and JS files, are displayed as a web page.
   * Integration of PHP along with HTML is also done in order to enhance the working of our project model. 


## Project Modules :

- _**Admin Module** :_ 👨‍⚖️
    - The admin is presented with the functionality to register new students and add their photos
to the training dataset. Managing their attendance and analysing them.
The admin is also responsible for managing the warden. 
    - Taking the warden attendance with the help of the facial recognition system and managing it.
The ‘View Attendance Reports’ button takes the student to the attendance management section
of the website.  
    - The attendance dashboard displays the attendance pattern for the previous and
current week, along with the current date’s statistics. 
    - The admin is presented with another tab
which allows filtering attendance by the student and viewing it in tabular as well as graphical
format. 
    - Another tab allows filtering the attendance data by date.

- _**Warden Module** :_ 👮‍♀️
    - The warden can see their attendance and analyse them. 
    - They can also be able to look into the student attendance and analyse it. 
    - Unlike admin, the warden will not be able to manage any other profiles and can only be able to analyse the attendance.

- _**Mark Attendance Module** :_ ✅ 
    - This is the heart module of our project where we have included the facial recognition system.The ‘Mark Your Attendance’ buttons provide the student with the functionality to mark
their time ins and time outs. These can be used multiple times a day and store the details of all
time-ins and time-outs recorded for each student. 
    - On clicking either of these buttons, the webcam
stream is activated, and any faces present in the field of view are detected, recognized, and
labeled with the corresponding name and confidence value. 
    - The attendance, time and name of
the person are recorded in the database. 

## Representation of the Dataset:

We chose Tabular Representation of Attendance Data. The application provides the admin with the feature to view attendance reports, where in attendance data is displayed as tables.
Data is fetched from the attendance database in the form of querysets. These query sets can be displayed as a table by using the {% for tag %} of
the templating language for displaying rows and outputting each attribute of the object in a
different table cell (column).


## Technologies Used:
 - Python
 - PHP
 - HTML
 - CSS
 - Bootstrap
 - Javascript

## Database Used:
 - MySQL

## Front End Contribution :
### Bootstrap,CSS,HTML :-
In order to develop the website’s UI and UX, Bootstrap and CSS styling were used. 
   * CSS- Cascading Style sheets are
used for describing the presentation of a document written in a markup language such as HTML.
It allows the addition of styles to a webpage with the use of classes and IDs. 
   - Bootstrap is a front
end framework that simplifies the process of adding styles, by providing built-in CSS classes that 
can be added to HTML elements, allowing the developer to forgo the process of writing
complicated CSS.
   * For this project, Bootstrap was added via the Bootstrap CDN. The url of Bootstrap CSS
files is simply added within the <link> tag of the HTML file, and Bootstrap classes can then be
used throughout the file. The Bootstrap typography, navbar, table, button, card, container, alert
classes were used extensively throughout the project. 

### Integration :-
   * The website have been designed using the HTML and CSS for giving it a great user friendly look. There should also exist the integration part where we not only check for the validations but also look for the design part to interact with the user. The use of bootstrap have been done inorder to make our website work well for the mobile users as well.
   - For the interaction of Front end with the backend database we have used PHP, which followed MTV architecture along with the HTML and CSS files. 
   * We have also used JQuery for the validations of our website. Javascript have also helped in authentication of the user as admin or as warden.

<!-- ## API Used: -->

<!-- ## Backend Databases:
Python allows developers to connect their applications with databases such as SQLite,
MySQL, PostgreSQL. Every database implements and uses SQL in a different manner. Django
takes care of the hassle of generating SQL specific to that database by providing you with the
ability to write Python data structures instead of the database language. Django includes an Object
Relational Mapping (ORM) layer that can be used for this interaction with data from various
relational databases like SQLite, PostgreSQL. Django also provides Models, which are Python
classes which act as a bridge between the database and the server. The following figure illustrates
how the Django ORM and Models, together implement traditional tables, rows and columns as
classes, objects, and attributes, making data a lot easier to work with. Data can now be fetched, created and saved as objects, and Django takes care of the SQL
for the developers.
This project uses the built-in User model provided by Django, which stores the information
of all students. This project also uses two other model classes ‘Present’ and ‘Time’ that store Attendance
(present/absent) and time-in, time-out, respectively of all students. -->
## Steps to run:-
1. Open the XAMPP Controller and start Apache and MySQL. Make sure the port number used by mysql and in the code is same.
2. After that, import the record.sql file in your MYSQL.
3. Once it is done, you can access index.html file.

## Screenshots:-

![Main Page](./screenshots/1.png?raw=true)

![Login for Admin](./screenshots/2.png?raw=true)

![Manage Student Page](./screenshots/4.png?raw=true)

![Warden Page](./screenshots/5.png?raw=true)

![Add new Warden Page](./screenshots/6.png?raw=true)

![Delete Warden Page](./screenshots/7.png?raw=true)

![Attendance Record](./screenshots/8.png?raw=true)

![Facial Recogntion](./screenshots/9.png?raw=true)
