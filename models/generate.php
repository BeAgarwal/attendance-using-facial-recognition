<?php
include('database.php');
    $database = new Database();
    $date = $_POST['date'];
   
    $result = $database->runQuery("SELECT ID, NAME, TIME FROM attendance where date='$date'");


$header = $database->runQuery("SELECT UCASE(`COLUMN_NAME`) 
FROM `INFORMATION_SCHEMA`.`COLUMNS` 
WHERE `TABLE_SCHEMA`='record' 
AND `TABLE_NAME`='attendance'
and `COLUMN_NAME` in ('ID','NAME','TIME')");

require('fpdf.php');
$pdf = new FPDF();
$pdf->AddPage();

class PDF extends FPDF
{
	function Header()
	{
        $this->Image('logo.png',10,6,20);
    $this->SetFont('Arial','B',15);
    	$this->Cell(75);
    
        $this->Cell(60,10,'ATTENDANCE',0,0,'L');
        $this->Line(75,20,135,20);
        $this->Ln(10);

            
    		$this->Ln(20);
	}

	function Footer()
	{
    
    		$this->SetY(-15);
    
    		$this->SetFont('Arial','I',8);
    
    		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);

foreach($header as $heading) {
	foreach($heading as $column_heading)
		$pdf->Cell(45,12,$column_heading);
}

foreach($result as $row) {
	$pdf->Ln();
	foreach($row as $column)
		$pdf->Cell(45,10,$column);
}
$pdf->Output('','attendance.pdf');
?>